import os

print("Eteignement de l'infrastructure")
os.system('docker-compose -p infrastructure down')

print("Eteignement de l'application")
os.system('docker-compose -f ./Application/docker-compose.yml -p application down')
