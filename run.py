import os

if os.path.isdir('./Application') == True:
  print("Le dossier Application exite déjà")
  print("Il ne sera pas cloner")

else:
  print("Clonage du projet application")
  os.system('git clone https://gitlab.com/Oxelio/appli-devops.git')
  os.rename('appli-devops', 'Application')

print("Lancement de l'infrastructure")
os.system('docker-compose -p infrastructure up -d')

print("Lancement de l'application")
os.system('docker-compose -f ./Application/docker-compose.yml -p application up -d')
