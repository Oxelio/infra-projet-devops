# Projet

Notre projet de la formation DevOps

## Pré-requis

Il faut avoir python et docker

## Démarrage

Nous allons lancer l'infrastructure et l'application

``` python
python .\run.py
```

## Configuration du GitLab

Pour utiliser le gitlab nous allons devoir le configurer. Pour cela nous devons d'abords configurer les runners puis ajouter les variables docker.

### 1. Enregistrer un Runner

```
    docker exec -it runner1 gitlab-runner register
    http://[mon_ip]:9000
    [Copier le token depuis Gitlab > Settings > CI/CD > Runners]
    [Entrer une courte description]
    [Tag si besoin]
    docker
    python
```

* Dans Gitlab > Setting > Settings > CI/CD > Runners
Editer le runner enregistré
Cocher Run untagged jobs si besoin

* Dans config.toml du runner ajouter
network_mode = "nom-du-reseau"
(le nom du réseau se trouve dans Docker > Networks)
et modifier privileged = true


### 2. Ajouter les variables à Gitlab


Dans Gitlab > Setting > Settings > CI/CD > Variables
Ajouter les variables en décochant "Protect variable" et en cochant "Mask variable"

|   |   |
|---|---|
| CI_REGISTRY | docker.io |
| CI_REGISTRY_IMAGE | index.docker.io/[mon-login-docker]/[mon-nom-projet] |
| CI_REGISTRY_USER | [mon-login-docker] |
| CI_REGISTRY_PASSWORD | [mon-mdp-docke]r |

## Arrêt

Pour arrêter l'infrastructure et l'application
``` python
python .\down.py
```

